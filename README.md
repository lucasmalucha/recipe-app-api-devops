# Terraform Recipes

## GitLab Branch

`git checkout -b feature/gitlab-ci-cd-skeleton`

## AWS VAULT & EC2

### Retrieve credentials

`aws-vault exec <account_name> --duration=12h -- cmd.exe`

### SSH to EC2 Bastion

`ssh ec2-user@<ec2 bastion host from pipeline>`

### Create superuser

`
docker run -it \
-e DB_HOST=<Host from pipeline job>\
-e DB_NAME=recipe \
-e DB_USER=recipeapp \
-e DB_PASS=<Password> \
<ECR arn> \
sh -c "python manage.py wait_for_db && python manage.py migrate && python manage.py createsuperuser"
`


## Docker-compose Terraform

### Init

`docker-compose -f deploy/docker-compose.yml run --rm terraform init`

### Format 

`docker-compose -f deploy/docker-compose.yml run --rm terraform fmt`

### Validate

`docker-compose -f deploy/docker-compose.yml run --rm terraform validate`

### Plan

`docker-compose -f deploy/docker-compose.yml run --rm terraform plan`

### Apply

`docker-compose -f deploy/docker-compose.yml run --rm terraform apply`

### Destroy

`docker-compose -f deploy/docker-compose.yml run --rm terraform destroy`

### Workspace List

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list`


### New Workspace

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace new <name>`

